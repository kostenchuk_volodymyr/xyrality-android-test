package com.xyrality.androidtest.presentation.worlds;

import com.xyrality.androidtest.Constants;
import com.xyrality.androidtest.models.Authorization;
import com.xyrality.androidtest.models.World;
import com.xyrality.androidtest.network.ServerAPI;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class WorldsListPresenterTest {

    private WorldsListPresenter.WorldsListLoadView mView;
    private WorldsListPresenter presenter;

    @Before
    public void setUp() {
        mView = mock(WorldsListPresenter.WorldsListLoadView.class);
        ServerAPI serverAPI = mock(ServerAPI.class);

        presenter = new WorldsListPresenter();
        presenter.bindView(mView);
        presenter.serverAPI = serverAPI;

        Constants.ANDROID_DEVICE_TYPE = "device_type";
        Constants.ANDROID_DEVICE_ID = "device_id";

        Scheduler immediate = new Scheduler() {
            @Override
            public Scheduler.Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitComputationSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }

    @Test
    public void testListLoadingWhenUnknownUser() {

        String error = "Unknown user";
        Authorization authorization = new Authorization();
        authorization.setError(error);

        when(presenter.serverAPI.authorization(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(Observable.just(authorization));

        presenter.loadWorldsList("email@email.com", "pass");
        verify(mView, times(1)).showLoading();
        verify(mView, times(1)).showError(error);
        verify(mView, times(1)).hideLoading();
        verify(mView, times(1)).hideRetry();
        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testListLoadingWhenListNotEmpty() {

        ArrayList<World> allAvailableWorlds = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            allAvailableWorlds.add(new World());
        }
        Authorization authorization = new Authorization();
        authorization.setWorldsList(allAvailableWorlds);

        when(presenter.serverAPI.authorization(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(Observable.just(authorization));

        presenter.loadWorldsList("email@email.com", "pass");
        verify(mView, times(1)).showLoading();
        verify(mView, times(1)).hideEmptyLayout();
        verify(mView, times(1)).worldsListLoaded(authorization);
        verify(mView, times(1)).hideLoading();
        verify(mView, times(1)).hideRetry();
        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testListLoadingWhenListEmpty() {

        when(presenter.serverAPI.authorization(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(Observable.just(new Authorization()));

        presenter.loadWorldsList("email@email.com", "pass");
        verify(mView, times(1)).showLoading();
        verify(mView, times(1)).showEmptyLayout();
        verify(mView, times(1)).hideLoading();
        verify(mView, times(1)).hideRetry();
        verifyNoMoreInteractions(mView);
    }
}
