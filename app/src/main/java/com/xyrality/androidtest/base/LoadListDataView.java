package com.xyrality.androidtest.base;

/**
 * Created by Kostenchuk V. on 25.08.2017.
 * vkosten4uk@gmail.com
 */
public interface LoadListDataView extends LoadDataView {

    void showEmptyLayout();

    void hideEmptyLayout();
}
