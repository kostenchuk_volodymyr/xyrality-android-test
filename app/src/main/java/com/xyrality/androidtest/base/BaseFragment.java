package com.xyrality.androidtest.base;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * Created by Kostenchuk V. on 25.08.2017.
 * vkosten4uk@gmail.com
 */
public abstract class BaseFragment<P> extends Fragment {

    @Nullable
    private P presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializePresenter();
    }

    protected abstract void initializePresenter();

    @NonNull
    protected P getPresenter() {
        if (presenter == null) {
            throw new IllegalStateException("Presenter should be created before of using it");
        }
        return presenter;
    }

    protected void setPresenter(@NonNull final P presenter) {
        this.presenter = presenter;
    }

    protected void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    protected void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        IBinder windowToken = null;
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            windowToken = view.getWindowToken();
        }
        if (windowToken != null) {
            inputManager.hideSoftInputFromWindow(windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } else {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
    }
}
