package com.xyrality.androidtest.base;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.xyrality.androidtest.R;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public abstract class BaseListFragment<P> extends BaseFragment<P> implements LoadListDataView {

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView listRecyclerView;
    private RelativeLayout retryRelativeLayout;
    private LinearLayout emptyListLinearLayout;
    private Button retryButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_list, container, false);

        swipeRefreshLayout = fragmentView.findViewById(R.id.srl_container);
        listRecyclerView = fragmentView.findViewById(R.id.rv_list);
        retryRelativeLayout = fragmentView.findViewById(R.id.rl_retry);
        retryButton = fragmentView.findViewById(R.id.btn_retry);
        emptyListLinearLayout = fragmentView.findViewById(R.id.ll_empty_list);

        initializeList();
        initializeSwipeRefreshLayout();
        initializeRetry();

        return fragmentView;
    }

    protected abstract void reloadData();

    protected void setAdapter(RecyclerView.Adapter adapter) {
        this.listRecyclerView.setAdapter(adapter);
    }

    private void initializeRetry() {
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reloadData();
            }
        });
    }

    private void initializeSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadData();
            }
        });
    }

    private void initializeList() {
        LinearLayoutManager listLayoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        this.listRecyclerView.setLayoutManager(listLayoutManager);
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showRetry() {
        retryRelativeLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        retryRelativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        showToastMessage(message);
    }

    @Override
    public void showEmptyLayout() {
        emptyListLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyLayout() {
        emptyListLinearLayout.setVisibility(View.GONE);
    }
}
