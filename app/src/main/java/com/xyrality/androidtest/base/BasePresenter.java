package com.xyrality.androidtest.base;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */

public class BasePresenter<V> {

    @Nullable
    private volatile V view;

    protected CompositeDisposable subscriptions;

    @CallSuper
    public void bindView(@NonNull V view) {
        this.view = view;
        this.subscriptions = new CompositeDisposable();
    }

    @NonNull
    protected V getView() {
        if (view == null) {
            throw new IllegalStateException("You can't interact with view before or after it attached/detached from view");
        }
        return view;
    }

    @CallSuper
    public void unbindView(@NonNull V view) {
        final V previousView = this.view;

        if (subscriptions != null) {
            subscriptions.clear();
        }

        if (previousView == view) {
            this.view = null;
        } else {
            throw new IllegalStateException("Unexpected view! previousView = " + previousView + ", view to unbind = " + view);
        }
    }
}
