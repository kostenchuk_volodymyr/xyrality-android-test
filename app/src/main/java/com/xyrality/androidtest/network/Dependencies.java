package com.xyrality.androidtest.network;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xyrality.androidtest.BuildConfig;
import com.xyrality.androidtest.Constants;

import java.lang.reflect.Modifier;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;

/**
 * Created by Kostenchuk V. on 25.08.2017.
 * vkosten4uk@gmail.com
 */
public final class Dependencies {

    static ServerAPI serverAPI;

    public static void init() {
        HttpLoggingInterceptor interceptor = provideHttpLoggingInterceptor();
        OkHttpClient client = provideOkHttpClientDefault(interceptor);

        serverAPI = provideRestApi(client);
    }

    public static ServerAPI getServerAPI() {
        return serverAPI;
    }

    static ServerAPI provideRestApi(@NonNull OkHttpClient okHttpClient) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT,
                Modifier.STATIC).create();
        final Retrofit.Builder builder = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        return builder.build().create(ServerAPI.class);
    }

    static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor =
                new HttpLoggingInterceptor(message -> Log.d(Constants.LOG_KEY, message));
        interceptor.setLevel(BuildConfig.DEBUG ? BODY : NONE);
        return interceptor;
    }

    static OkHttpClient provideOkHttpClientDefault(HttpLoggingInterceptor interceptor) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.interceptors().add(interceptor);
        clientBuilder.interceptors().add(chain -> {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder();
            builder.addHeader("Accept", "application/json");
            return chain.proceed(builder.build());
        });
        return clientBuilder.build();
    }
}
