package com.xyrality.androidtest.network;

import com.xyrality.androidtest.models.Authorization;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Kostenchuk V. on 25.08.2017.
 * vkosten4uk@gmail.com
 */
public interface ServerAPI {

    String AUTHORIZATION = "/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds";

    @FormUrlEncoded
    @POST(AUTHORIZATION)
    Observable<Authorization> authorization(@Field("login") String login,
                                            @Field("password") String password,
                                            @Field("deviceType") String deviceType,
                                            @Field("deviceId") String deviceId);
}
