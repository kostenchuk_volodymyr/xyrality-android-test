package com.xyrality.androidtest;

/**
 * Created by Kostenchuk V. on 25.08.2017.
 * vkosten4uk@gmail.com
 */
public class Constants {

    public static final String LOG_KEY = "LOGALL";

    public static final String BASE_URL = "http://backend1.lordsandknights.com/";
    public static String ANDROID_DEVICE_ID;
    public static String ANDROID_DEVICE_TYPE;
}
