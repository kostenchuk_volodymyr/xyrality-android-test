package com.xyrality.androidtest.utils;

import android.content.Context;

import com.xyrality.androidtest.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class Validator {

    private final static int MIN_LENGTH = 1;
    private final static int MAX_LENGTH = 50;

    public static String validateEmail(Context context, String email) {
        String error = null;

        if (email.length() < MIN_LENGTH) {
            error = context.getString(R.string.validator_email_is_required);
        } else if (email.length() > MAX_LENGTH) {
            error = context.getString(R.string.validator_email_is_too_long);
        } else if (!isEmailValid(email)) {
            error = context.getString(R.string.validator_email_is_not_valid);
        }

        return error;
    }

    private static boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        }

        String regExp =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExp, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public static String validatePassword(Context context, String password) {
        String error = null;

        if (!password.matches("^.{7,50}$")) {
            error = context.getString(R.string.validator_password_have_to_contain_characters);
        }

        return error;
    }
}
