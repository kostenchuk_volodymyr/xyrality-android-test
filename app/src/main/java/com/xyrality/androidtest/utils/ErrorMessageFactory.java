package com.xyrality.androidtest.utils;

import android.accounts.AuthenticatorException;
import android.content.Context;

import com.xyrality.androidtest.R;

import java.net.UnknownHostException;

import retrofit2.HttpException;

/**
 * Created by Kostenchuk V. on 25.08.2017.
 * vkosten4uk@gmail.com
 */
public class ErrorMessageFactory {

    private static final int NOT_FOUND_STATUS_CODE = 404;
    private static final int BAD_REQUEST_STATUS_CODE = 400;

    public static String create(Context context, Throwable exception) {
        String message = context.getString(R.string.exception_message_generic);

        if (exception instanceof HttpException) {
            HttpException error = (HttpException) exception;
            if (error.response() != null && error.response().code() == NOT_FOUND_STATUS_CODE) {
                message = context.getString(R.string.exception_message_not_found_web_data_error);
            } else if (error.response() != null && error.response().code() == BAD_REQUEST_STATUS_CODE) {
                message = context.getString(R.string.exception_message_credentials);
            } else {
                message = context.getString(R.string.exception_message_loading_web_data_error);
            }
        } else if (exception instanceof UnknownHostException) {
            message = context.getString(R.string.exception_message_internet_connection_error);
        } else if (exception instanceof AuthenticatorException) {
            message = context.getString(R.string.exception_message_authentication);
        }

        return message;
    }
}
