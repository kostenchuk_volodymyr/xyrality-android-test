package com.xyrality.androidtest;

import android.os.Bundle;

import com.xyrality.androidtest.base.BaseActivity;
import com.xyrality.androidtest.presentation.login.LoginFragment;
import com.xyrality.androidtest.presentation.worlds.WorldsListFragment;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class MainActivity extends BaseActivity implements LoginFragment.NavigationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_toolbar);
        if (savedInstanceState == null) {
            addFragment(R.id.fl_container, LoginFragment.newInstance());
        }
    }

    @Override
    public void moveToWorldsList(String email, String password) {
        replaceFragment(R.id.fl_container, WorldsListFragment.newInstance(email, password));
    }
}
