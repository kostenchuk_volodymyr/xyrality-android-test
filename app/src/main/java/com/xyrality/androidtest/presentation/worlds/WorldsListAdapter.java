package com.xyrality.androidtest.presentation.worlds;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xyrality.androidtest.R;
import com.xyrality.androidtest.models.World;

import java.util.List;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class WorldsListAdapter extends RecyclerView.Adapter<WorldsListAdapter.ItemViewHolder> {

    private List<World> itemCollection;
    private LayoutInflater layoutInflater;

    public WorldsListAdapter(Context context, List<World> itemCollection) {
        if (context != null) {
            this.layoutInflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.itemCollection = itemCollection;
        }
    }

    @Override
    public int getItemCount() {
        return (this.itemCollection != null) ? this.itemCollection.size() : 0;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemViewHolder itemViewHolder = null;
        if (this.layoutInflater != null) {
            itemViewHolder =
                    new ItemViewHolder(this.layoutInflater.inflate(R.layout.item_world, parent, false));
        }
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {

        holder.nameTextView.setText(itemCollection.get(position).getName());
        holder.nameTextView.setBackgroundResource(
                position % 2 == 0 ? R.color.pale_grey : android.R.color.white);
    }

    public void setCollection(List<World> itemCollection) {
        this.itemCollection = itemCollection;
        this.notifyDataSetChanged();
    }

    public void clearCollection(){
        itemCollection = null;
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView nameTextView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.tv_name);
        }
    }
}