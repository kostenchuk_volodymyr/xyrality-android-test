package com.xyrality.androidtest.presentation.worlds;

import android.content.Context;
import android.text.TextUtils;

import com.xyrality.androidtest.Constants;
import com.xyrality.androidtest.base.BasePresenter;
import com.xyrality.androidtest.base.LoadListDataView;
import com.xyrality.androidtest.models.Authorization;
import com.xyrality.androidtest.network.Dependencies;
import com.xyrality.androidtest.network.ServerAPI;
import com.xyrality.androidtest.utils.ErrorMessageFactory;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class WorldsListPresenter extends BasePresenter<WorldsListPresenter.WorldsListLoadView> {

    public ServerAPI serverAPI;

    public WorldsListPresenter() {
        serverAPI = Dependencies.getServerAPI();
    }

    public void loadWorldsList(String email, String password) {
        WorldsListLoadView worldsListLoadView = getView();
        worldsListLoadView.showLoading();

        subscriptions.add(serverAPI.authorization(
                email, password, Constants.ANDROID_DEVICE_TYPE, Constants.ANDROID_DEVICE_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(authorization -> {
                    if (authorization.getError() == null || TextUtils.isEmpty(authorization.getError())) {
                        if (authorization.getWorldsList().size() > 0) {
                            worldsListLoadView.hideEmptyLayout();
                            worldsListLoadView.worldsListLoaded(authorization);
                        } else {
                            worldsListLoadView.showEmptyLayout();
                        }
                    } else {
                        worldsListLoadView.showError(authorization.getError());
                    }
                    worldsListLoadView.hideLoading();
                    worldsListLoadView.hideRetry();
                }, throwable -> {
                    worldsListLoadView.hideLoading();
                    worldsListLoadView.showRetry();
                    showErrorMessage(throwable);
                }));
    }

    private void showErrorMessage(Throwable errorBundle) {
        Context context = this.getView().getContext();
        if (context != null) {
            String errorMessage = ErrorMessageFactory.create(context, errorBundle);
            this.getView().showError(errorMessage);
        }
    }

    interface WorldsListLoadView extends LoadListDataView {
        void worldsListLoaded(Authorization authorization);
    }
}
