package com.xyrality.androidtest.presentation.worlds;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.xyrality.androidtest.base.BaseListFragment;
import com.xyrality.androidtest.models.Authorization;


/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class WorldsListFragment extends BaseListFragment<WorldsListPresenter> implements WorldsListPresenter.WorldsListLoadView {

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    public static WorldsListFragment newInstance(String email, String password) {
        WorldsListFragment worldsListFragment = new WorldsListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EMAIL, email);
        bundle.putString(PASSWORD, password);
        worldsListFragment.setArguments(bundle);
        return worldsListFragment;
    }

    private String email;
    private String password;
    private WorldsListAdapter worldsListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        email = bundle.getString(EMAIL);
        password = bundle.getString(PASSWORD);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        worldsListAdapter.clearCollection();
    }

    @Override
    protected void initializePresenter() {
        WorldsListPresenter worldsListPresenter = new WorldsListPresenter();
        worldsListPresenter.bindView(this);
        setPresenter(worldsListPresenter);
    }

    @Override
    protected void reloadData() {
        loadList();
    }

    private void loadList() {
        getPresenter().loadWorldsList(email, password);
    }

    @Override
    public void worldsListLoaded(Authorization authorization) {
        if (worldsListAdapter == null) {
            worldsListAdapter = new WorldsListAdapter(getActivity(), authorization.getWorldsList());
            setAdapter(worldsListAdapter);
        } else {
            worldsListAdapter.setCollection(authorization.getWorldsList());
        }
    }
}
