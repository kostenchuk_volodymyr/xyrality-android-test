package com.xyrality.androidtest.presentation.login;

import android.content.Context;

import com.xyrality.androidtest.base.BasePresenter;
import com.xyrality.androidtest.utils.Validator;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class LoginPresenter extends BasePresenter<LoginPresenter.LoginEditView> {

    private Context context;

    public LoginPresenter(Context context) {
        this.context = context;
    }

    public boolean checkEmail(String email) {
        String error = Validator.validateEmail(context, email);

        LoginEditView loginEditView = getView();

        if (error == null) {
            loginEditView.emailDataAccepted();
            return true;
        } else {
            loginEditView.emailDataError(error);
            return false;
        }
    }

    public boolean checkPassword(String password) {
        String error = Validator.validatePassword(context, password);

        LoginEditView loginEditView = getView();

        if (error == null) {
            loginEditView.passwordDataAccepted();
            return true;
        } else {
            loginEditView.passwordDataError(error);
            return false;
        }
    }

    public void validateLoginData(String userEmail, String password) {
        if (checkEmail(userEmail) && checkPassword(password)) {
            this.getView().loginDataValidated();
        }
    }

    public interface LoginEditView {

        void emailDataError(String message);

        void emailDataAccepted();

        void passwordDataError(String message);

        void passwordDataAccepted();

        void loginDataValidated();
    }
}
