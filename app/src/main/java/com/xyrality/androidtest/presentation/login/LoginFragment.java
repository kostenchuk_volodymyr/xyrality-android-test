package com.xyrality.androidtest.presentation.login;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.xyrality.androidtest.R;
import com.xyrality.androidtest.base.BaseFragment;

/**
 * Created by Kostenchuk V. on 27.08.2017.
 * vkosten4uk@gmail.com
 */
public class LoginFragment extends BaseFragment<LoginPresenter> implements View.OnClickListener, LoginPresenter.LoginEditView {

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    private EditText emailEditText;
    private EditText passwordEditText;
    private Button goButton;
    private TextWatcher emailTextWatcher;
    private TextWatcher passwordTextWatcher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_login, container, false);
        emailEditText = fragmentView.findViewById(R.id.et_email);
        passwordEditText = fragmentView.findViewById(R.id.et_pass);
        goButton = fragmentView.findViewById(R.id.btn_go);

        initWatchers();

        emailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    goButton.performClick();
                }
                return false;
            }
        });

        goButton.setOnClickListener(this);
        return fragmentView;
    }


    private void initWatchers() {
        emailTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkEmail();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };

        passwordTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkPassword();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };

        emailEditText.addTextChangedListener(emailTextWatcher);
        passwordEditText.addTextChangedListener(passwordTextWatcher);
    }

    private void checkEmail() {
        if (emailEditText != null) {
            getPresenter().checkEmail(emailEditText.getText().toString());
        }
    }

    private void checkPassword() {
        if (passwordEditText != null) {
            getPresenter().checkPassword(passwordEditText.getText().toString());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        emailEditText.removeTextChangedListener(emailTextWatcher);
        passwordEditText.removeTextChangedListener(passwordTextWatcher);
        getPresenter().unbindView(this);
    }

    @Override
    protected void initializePresenter() {
        LoginPresenter loginPresenter = new LoginPresenter(getContext());
        loginPresenter.bindView(this);
        setPresenter(loginPresenter);
    }

    @Override
    public void onClick(View view) {
        hideKeyboard();
        getPresenter().validateLoginData(emailEditText.getText().toString(),
                passwordEditText.getText().toString());
    }

    @Override
    public void emailDataError(String message) {
        emailEditText.setError(message);
        emailEditText.requestFocus();
    }

    @Override
    public void emailDataAccepted() {
        emailEditText.setError(null);
    }

    @Override
    public void passwordDataError(String message) {
        passwordEditText.setError(message);
        passwordEditText.requestFocus();
    }

    @Override
    public void passwordDataAccepted() {
        passwordEditText.setError(null);
    }

    @Override
    public void loginDataValidated() {
        ((NavigationListener) getActivity())
                .moveToWorldsList(emailEditText.getText().toString(), passwordEditText.getText().toString());
    }

    public interface NavigationListener {
        void moveToWorldsList(String email, String password);
    }
}
