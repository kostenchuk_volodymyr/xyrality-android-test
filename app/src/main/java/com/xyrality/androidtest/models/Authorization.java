
package com.xyrality.androidtest.models;

import java.util.ArrayList;
import java.util.List;

public class Authorization {

    private boolean featureAppleTVSynchronisation;
    private boolean featureDirectPlay;
    private boolean googleLoginSwitchOn;
    private String serverVersion;
    private boolean facebookLoginSwitchOn;
    private ArrayList<World> allAvailableWorlds = new ArrayList<>();
    private boolean featureHelpshift;
    private String time;
    private String info;
    private String error;

    public boolean isFeatureAppleTVSynchronisation() {
        return featureAppleTVSynchronisation;
    }

    public boolean isFeatureDirectPlay() {
        return featureDirectPlay;
    }

    public boolean isGoogleLoginSwitchOn() {
        return googleLoginSwitchOn;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public boolean isFacebookLoginSwitchOn() {
        return facebookLoginSwitchOn;
    }

    public List<World> getWorldsList() {
        return allAvailableWorlds;
    }

    public boolean isFeatureHelpshift() {
        return featureHelpshift;
    }

    public String getTime() {
        return time;
    }

    public String getInfo() {
        return info;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setWorldsList(ArrayList<World> allAvailableWorlds) {
        this.allAvailableWorlds = allAvailableWorlds;
    }
}
