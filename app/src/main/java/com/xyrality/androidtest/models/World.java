
package com.xyrality.androidtest.models;

public class World {

    private String name;
    private String mapURL;
    private WorldStatus worldStatus;
    private String country;
    private String language;
    private String id;
    private String url;

    public String getName() {
        return name;
    }

    public String getMapURL() {
        return mapURL;
    }

    public WorldStatus getWorldStatus() {
        return worldStatus;
    }

    public String getCountry() {
        return country;
    }

    public String getLanguage() {
        return language;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
