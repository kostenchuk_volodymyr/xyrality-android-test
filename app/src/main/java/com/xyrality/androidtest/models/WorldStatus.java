
package com.xyrality.androidtest.models;

public class WorldStatus {

    private long id;
    private String description;

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
