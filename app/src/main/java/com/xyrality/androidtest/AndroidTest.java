package com.xyrality.androidtest;

import android.app.Application;
import android.provider.Settings;

import com.xyrality.androidtest.network.Dependencies;

/**
 * Created by Kostenchuk V. on 25.08.2017.
 * vkosten4uk@gmail.com
 */
public class AndroidTest extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Dependencies.init();

        Constants.ANDROID_DEVICE_ID = Settings.Secure.getString(getBaseContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Constants.ANDROID_DEVICE_TYPE = String.format("%s %s",
                android.os.Build.MODEL, android.os.Build.VERSION.RELEASE);
    }
}
